# signstar

A signing service serves as frontend to a tamperproof hardware appliance (e.g. NetHSM).
Various clients are each assigned an individual signing key through the signing service, which the signing service uses on their behalf to sign digests or entire files.

### Signing service

The signing service manages a list of operator credentials to the hardware appliance and maps those to credentials to the signing service in a one-to-one relation.
Clients connect to the signing service using individual Unix users over SSH, who are forced to call a single executable of the signing service.
An attestation log is appended to for signing operations. This can be realized using a git repository as backend, by appending message-only commits (`git commit --allow-empty`).
When creating OpenPGP signatures, the signing service appends the transaction's metadata in the signature as notation (e.g. attestation log position, file metadata, requesting client).

### Signing service clients

All signing service clients store the private key material for a *dedicated* SSH key pair used for connecting to the signing service in tamper proof hardware (e.g. TPM, security token).
Build servers expose successfully signed packages via a static webserver for `n` days.

### Hardware appliance

The hardware appliance offers an admin and various operator accounts.
The admin credentials are only needed for initial setup of the appliance and creating or removing operator accounts.
Each operator account is assigned *one* private signing key of the hardware appliance (via tags).

### OpenPGP specifics

All configured private keys in the hardware appliance are added as signing subkeys to one OpenPGP primary key each.
The OpenPGP primary key is used to manage the lifecycle of a signing subkey: The expiration time of the subkey is relying solely on the expiration time of the primary key.
The OpenPGP primary keys are managed entirely offline and their private key material is only required when adding a signing subkey or when extending their expiration period.

## Implementation

In this setup a microservice takes care of taking authenticated client requests and issuing signatures for the request via a PKCS#11 backend.
Clients may send checksums or entire files using a custom wire format.

On a build server the signed packages are exposed via a static webserver location.

```mermaid
---
title: Signing service signing files or hashes
---
sequenceDiagram
    participant B as build server
    participant S as signing server
    participant N as NetHSM
    participant R as repo server
    participant L as logging server
    participant M as metrics server

    Note over B: 1 signing server credential
    Note over S: n NetHSM operator credentials,<br/>n NetHSM certificate IDs,<br/>n client to NetHSM mappings
    Note over S: PKCS11 based tooling for signing
    Note over R: 1 signing server credential

    critical
        B ->> B: get sources,<br/>build package(s)
        loop get signature for each package
            critical
            option
                B ->> B: create checksum,<br/>gather metadata
                B ->> S: authenticate,<br/>send digest and metadata
            option
                B ->> B: gather metadata
                B ->> S: authenticate,<br/>send file and metadata
                S ->> S: create checksum for file
            end
            S ->> S: combine checksum and metadata to OpenPGP digest
            S ->> N: authenticate for client,<br/>transmit OpenPGP digest and cert ID
            N -->> S: raw cryptographic signature
            S ->> S: create OpenPGP signature
            S -->> B: receive OpenPGP signature
            B ->> B: move package file and OpenPGP signature<br/>to publicly accessible storage
        end
    end

    critical repository update
        R ->> R: order to add package(s) from build server to repo
        B ->> R: download package(s) and OpenPGP signature(s) to pool
        R ->> R: generate temporary sync databases
        loop get signature for each sync database
            critical
            option
                R ->> R: create checksum,<br/>gather metadata
                R ->> S: authenticate,<br/>send digest and metadata
            option
                R ->> R: gather metadata
                R ->> S: authenticate,<br/>send file and metadata
                S ->> S: create checksum for file
            end
            S ->> S: combine checksum and metadata to OpenPGP digest
            S ->> N: authenticate for client,<br/>transmit OpenPGP digest and cert ID
            N -->> S: raw cryptographic signature
            S ->> S: create OpenPGP signature
            S -->> R: receive OpenPGP signature
        end
        R ->> R: update repository
    end

    loop metrics collection
        B --> M: read
        S --> M: read
        N --> M: read
        R --> M: read
    end
    loop log aggregation
        N -->> L: send via syslog
    end
```

The signing process in more detail may look as follows:

```mermaid
---
title: Signing process in "Signing service signing files or hashes" scenario
---
sequenceDiagram
    participant C as n clients
    participant S as signing server
    participant A as attestation log
    participant N as NetHSM

    Note over C: one signing server credential each
    Note over S: n NetHSM operator credentials,<br/>n NetHSM certificate IDs,<br/>n client to NetHSM mappings

    critical authentication
        critical
        option
            C ->> C: create checksum, gather metadata
            C ->>+ S: authenticate,<br/>transmit hash and metadata
        option
            C ->> C: gather metadata
            C ->>+ S: authenticate,<br/>transmit file and metadata
        end
            S ->> A: log user access and request
    option login failure
        S -x C: failure
        S ->> A: log user login failure
    option login successful
        S ->> A: log user login success
        critical user mapping
            S ->> S: map client user to NetHSM user and certificate ID
            option mapping not found
                S ->> A: log user mapping failure
                S -x C: failure
            option mapping found
                S ->> A: log user mapping success
        end
    end
    critical data preparation
        S ->> S: check request
    option request is file
        S ->> S: create checksum for file
    end
    critical signing
        S ->>+ N: authenticate for client,<br/>transmit checksum and cert ID
    option authentication fails/ signature not created
        S ->> A: log signature failure
        S -x C: failure
        S ->> A: log signature return failure
    option authentication succeeds/ signature created
        N -->>- S: receive raw cryptographic signature
        S ->> A: log signature success
        S ->> S: create OpenPGP signature
        S -->>- C: receive OpenPGP signature
    end
```

Here, the `n clients` may be build servers or the repository server, as they are functionally equal in behavior.

